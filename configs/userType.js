const USER = "USER";
const ADMIN = "ADMIN";
const BOTH = "BOTH";

export default {
    USER,
    ADMIN,
    BOTH
  };
  