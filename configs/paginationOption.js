export const getOptionVariables = (limit, page) => {
    return {
      limit,
      page,
      sort: {
        updatedAt: -1,
      },
    };
  };
  