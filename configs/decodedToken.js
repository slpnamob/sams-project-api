const jwt = require('jsonwebtoken');

require("dotenv").config();

const decodedToken = (token) => {

  if (token) {
    const encoded = token.replace('Bearer ', '');
    const decoded = jwt.verify(encoded, process.env.JWT_KEY);
    return decoded;
  }
  return null
}
module.exports = { decodedToken }