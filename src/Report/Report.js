import ReportModel from "../../models/Report.js";
import { ApolloError } from "apollo-server";
import { getOptionVariables } from "../../configs/paginationOption.js";
import UserModel from "../../models/User.js";
import PostModel from "../../models/Post.js"

export class Report {
  static async getAllReports(root, limit = Number.MAX_SAFE_INTEGER, page = 1, context) {
    return await ReportModel.paginate(
      {is_deleted: false},
      {
        ...getOptionVariables(limit, page),
        populate: [
          { path: "createUser", model: "user", sort: {createdAt : -1} },
          { path: "reportPostId", model: "post", sort: {createdAt : -1} },
        ],
        sort: {createdAt : -1},
      },
      async (err, result) => {
        if (err) {
          throw err;
        }
        return {
          pagination: {
            limit,
            page,
            totalDocs: result.totalDocs,
            totalPages: result.totalPages,
          },
          reports: result.docs,
        };
      }
    );
  }

  static async getAllPostsThatHaveReport(limit = Number.MAX_SAFE_INTEGER, page = 1){
    const report_data = await ReportModel.find({is_deleted: false},null,{sort: {postId: 1}});
    var result = [];
    var onePost = {};
    var count = 1;
    var tmpReportPostId=undefined;
    for (const ele of report_data){
      if(JSON.stringify(tmpReportPostId) === JSON.stringify(ele.reportPostId)){
        count += 1;
      }
      else{
        if(tmpReportPostId != undefined){
          onePost.numberOfReport = count;
          result.push(onePost);
          count = 1;
        }
        const this_post = await PostModel.findOne({_id : ele.reportPostId, is_deleted:false},{_id: 1, name: 1, createUser: 1});
        onePost={
          postId: this_post._id,
          postName: this_post.name,
          creatorName: this_post.createUser
        }
        tmpReportPostId = ele.reportPostId;
      }
    }
    if(JSON.stringify(tmpReportPostId) === JSON.stringify(report_data[report_data.length-1].reportPostId)){
      onePost.numberOfReport = count;
      result.push(onePost);
    }
    return result;
  }

  static async getAllReportsFromThisPost(limit = Number.MAX_SAFE_INTEGER, page = 1, input) {
    const {postId} = input;
    const report_data = await ReportModel.paginate(
      {reportPostId : postId, is_deleted: false},
      {
        ...getOptionVariables(limit, page),
        populate: [
          { path: "createUser", model: "user", sort: {createdAt : -1} },
          { path: "reportPostId", model: "post", sort: {createdAt : -1} },
        ],
        sort: {createdAt : -1},
      },
      async (err, result) => {
        if (err) {
          throw err;
        }
        return {
          pagination: {
            limit,
            page,
            totalDocs: result.totalDocs,
            totalPages: result.totalPages,
          },
          reports: result.docs,
        };
      }
    );

    const post_data = await PostModel.findOne({_id : postId, is_deleted:false},{name: 1});

    for(const ele of report_data.reports){
      ele.postName = post_data.name;
    }

    return report_data;
  }


  static async createReport(input, user) {
    try{
      const { userId } = user;
      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      const { reportPostId } = input;
      input.createUser = userId;
      const post_data = await PostModel.findOne({ _id: reportPostId });
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if(post_data.createUser == userId) throw new ApolloError("You are the creator of this post can't report your post","CAN_NOT_REPORT");
      const report_new = new ReportModel(input);
      await report_new.save();
      return await ReportModel.findOne({ _id: report_new._id }).populate([
        { path: "createUser", model: "user" , sort: {createdAt : -1}},
        { path: "reportPostId", model: "post", sort: {createdAt : -1} },
      ]);
    }
    catch (err) { throw new ApolloError(err); }
  }
}