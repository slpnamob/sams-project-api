import {Report} from "./Report.js";
import { withUser,allUser } from "../../middleware/auth.js";
import userType from "../../configs/userType.js";

export default {
    queries: [
        {
            name: "getAllReports",
            typeDefs: `
            type Report{
                _id: ID!,
                comment: String,
                createUser: User!,
                reportPostId: Post!,
            },
            type AllReport {
                pagination: Pagination,
                reports : [Report],
            }`,
            returnTypeDef: `AllReport`,
            params: `limit: Int, page: Int`,
            action: async (root,{limit, page},context) =>
            withUser(root, {limit, page}, context, {
                userType: userType.ADMIN,
              })(async (parent, args, context, user) =>
                Report.getAllReports(limit, page)
              ),
        },
        {
            name: "getAllPostsThatHaveReport",
            typeDefs: `
            type PostThatHaveReport{
                postId: String,
                postName: String,
                numberOfReport: Int,
                creatorName: String,
            },
            `,
            returnTypeDef: `[PostThatHaveReport]`,
            params: `limit: Int, page: Int`,
            action: async (root,{limit, page},context) =>
            withUser(root, {limit, page}, context, {
                userType: userType.ADMIN,
              })(async (parent, args, context, user) =>
                Report.getAllPostsThatHaveReport(limit, page)
              ),
        },
        {
            name: "getAllReportsFromThisPost",
            typeDefs: `
                input getAllReportsFromThisPost{
                    postId: String!
                }
                type ReportFromPost{
                    _id: ID!,
                    comment: String,
                    createUser: User!,
                    reportPostId: Post!,
                    postName: String!,
                },
                type AllReportFromPost {
                    pagination: Pagination,
                    reports : [ReportFromPost],
                }`,
            returnTypeDef: `AllReportFromPost`,
            params: `limit: Int, page: Int, input: getAllReportsFromThisPost`,
            action: async (root ,{limit, page,input} , context) =>
            withUser(root, {limit, page,input}, context, {
                userType: userType.ADMIN,
              })(async (parent, args, context, user) =>
                Report.getAllReportsFromThisPost(limit, page,input)
              ),

        }
    ],
    mutations: [
        {
            name: "createReport",
            typeDefs:`
                input createReportInput{
                    comment: String!,
                    reportPostId: String!,
                }`,
            returnTypeDef: `Report`,
            params: `input: createReportInput`,
            action: async (root ,{input} , context) =>
            withUser(root, {input}, context, {
                userType: userType.BOTH,
              })(async (parent, args, context, user) =>
                Report.createReport(input, user)
              ),
        }
    ]}
    