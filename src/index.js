import user from "./User/UserController.js";
import postforCreator from "./Post/PostforCreator/PostController.js";
import postforUser from "./Post/PostforUser/PostController.js";
import review from "./Review/ReviewController.js";
import report from "./Report/ReportController.js";


export default [
    user,
    postforCreator,
    postforUser,
    review,
    report,
];