import mongoose from "mongoose";
import UserModel from "../../models/User.js";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { getOptionVariables } from "../../configs/paginationOption.js";
import { ApolloError } from "apollo-server";

export class User {
    static async getAllUsers(root,limit = Number.MAX_SAFE_INTEGER, page = 1,context){
        return await UserModel.paginate(
            {},
            {
              ...getOptionVariables(limit, page),
              populate: [
                { path: "posts", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
                { path: "joins", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
                { path: "favs", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
                { path: "history", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
                { path: "reviews", model: "review", sort: {createdAt : -1}},
              ],
              sort: {createdAt : -1},
            },
            async (err, result) => {
              if (err) {
                throw err;
              }
              return {
                pagination: {
                  limit,
                  page,
                  totalDocs: result.totalDocs,
                  totalPages: result.totalPages,
                },
                users: result.docs,
              };
            }
          );    
    }

    static async oneUser (user){
      try {
        const { userId } = user;
        return await UserModel.findOne({_id: userId}).populate([
          { path: "posts", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
          { path: "joins", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
          { path: "favs", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
          { path: "history", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
          { path: "reviews", model: "review", sort: {createdAt : -1}},
        ]
       );
    }
      catch (err) { throw new ApolloError("ไม่มีผู้ใช้งานนี้ในระบบ",err); }
    }

    static async register(input, context)
    {
        const {type, name, studentId, major, phoneNumber, email, password} = input;
        console.log(email.split("@").pop());
        if(email.split("@").pop() != "kmitl.ac.th") throw new ApolloError("E-mail ไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง");
        if(type=='student'){
          if(email.split("@",1) != studentId) throw new ApolloError("รหัสนักศึกษาไม่ตรงกับ E-mail กรุณาตรวจสอบอีกครั้ง");
        }
        var newUserSchema = {
          type: type,
          name: name,
          major: major,
          phoneNumber:phoneNumber,
          email: email,
          password: bcrypt.hashSync(password, 3),
          studentId: studentId,
        }
        const newUser = new UserModel(newUserSchema);
        await newUser.save();
        const forEncode = {
          userId : newUser._id,
          type: type,
          name: name,
          email: email,
          major: major,
          phoneNumber:phoneNumber,
          tokenType: "ACCESS_TOKEN",
          dateToken: Date.now(),
        }
        return { accessToken: jwt.sign(forEncode, "SAMS-project^-^")};
    }

    static async login(input, context)
    {
      const {email, password} = input;
      const [theUser] = await UserModel.find({email: email});
      if (!theUser) throw new ApolloError("ไม่มีผู้ใช้งานนี้ในระบบ");
      const isMatch = bcrypt.compareSync(password, theUser.password);
      if (!isMatch) throw new ApolloError("รหัสผ่านไม่ถูกต้อง");
      const forEncode = {
        userId : theUser._id,
        type: theUser.type,
        name: theUser.name,
        email: theUser.email,
        major: theUser.major,
        phoneNumber:theUser.phoneNumber,
        tokenType: "ACCESS_TOKEN",
        dateToken: Date.now(),
      }
      return {accessToken : jwt.sign(forEncode, "SAMS-project^-^")};
    }

    static async changePassword(input){
      const {code,newPassword} = input;
      const user_data = await UserModel.findOne({_id: code});
      if (!user_data) {
        throw new ApolloError("code ไม่ถูกต้อง", "WRONG_CODE");
      }
      user_data.password = bcrypt.hashSync(newPassword, 3);
      await user_data.save();
      return await UserModel.findOne({_id: code}).populate([
        { path: "posts", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
        { path: "joins", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
        { path: "favs", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
        { path: "history", model: "post", match: {is_deleted: false}, sort: {createdAt : -1}},
        { path: "reviews", model: "review", sort: {createdAt : -1}},
      ]
     );
    }
}
