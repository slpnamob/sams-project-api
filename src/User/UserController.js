import { User } from "./User.js";
import { withUser } from "../../middleware/auth.js";
import userType from "../../configs/userType.js";

export default {
    queries: [
      {
          name: "getAllUsers",
          typeDefs: `
            type Pagination {
                limit: Int
                page: Int,
                totalItem: Int,
                totalPages: Int,
            },
            type User{
                _id: ID!,
                type: String!,
                name: String!,
                studentId: String,
                major: String,
                phoneNumber: String!,
                email: String!,
                posts: [Post],
                joins: [Post],
                favs: [Post],
                history: [Post],
                reviews: [Review],
            },
            type AllUser {
                pagination: Pagination,
                users: [User],
            }`,
          returnTypeDef: `AllUser`,
          params: `limit: Int, page: Int`,
          action: async (root,{limit, page},context) =>
          User.getAllUsers(root,limit,page,context),
      },
      {
        name: "getOneUser",
        typeDefs: `
        `,
        params: ``,
        returnTypeDef: `User`,
        action: async (root, params, context) =>
          withUser(root, params, context, {
            userType: userType.BOTH,
          })(async (parent, args, context, user) =>
            User.oneUser(user)
          ),
      },
      
    ],
    mutations: [
        {
        name: "register",
        typeDefs: `
            input registerInput{
                type: String!,
                name: String!,
                studentId: String,
                major: String,
                phoneNumber: String!,
                email: String!,
                password: String!,
            },
            type Token{
                accessToken : String, 
            } `,
        params: `input: registerInput`,
        returnTypeDef: `Token`,
        action: async (root ,{input} , context) =>
            User.register(input, context)
      },
      {
        name: "login",
        typeDefs: `
            input loginInput{
                email: String!,
                password: String!,
            }`,
        params: `input: loginInput`,
        returnTypeDef: `Token`,
        action: async (root ,{input} , context) =>
            User.login(input, context)
      },
      {
          name: "editPhoto",
          typeDefs: `
            input editPhotoInput{
              photo: String!,
            }`,
          params: `input: editPhotoInput`,
          returnTypeDef: `User`,
          action: async (root, {input}, context) =>
            withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              User.editPhoto(input , user)
            ),
      },
      {
        name: "changePassword",
        typeDefs: `
          input changePasswordInput{
            code: String!,
            newPassword: String!,
          }`,
        params: `input: changePasswordInput`,
        returnTypeDef: `User`,
        action: async (root ,{input}) =>
            User.changePassword(input)
    },
      //edit profile รูป รหัสผ่าน
    ],
};