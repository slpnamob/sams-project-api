import ReviewModel from "../../models/Review.js";
import { ApolloError } from "apollo-server";
import { getOptionVariables } from "../../configs/paginationOption.js";
import UserModel from "../../models/User.js";
import PostModel from "../../models/Post.js"

export class Review {
  static async getAllReviews(root, limit = Number.MAX_SAFE_INTEGER, page = 1, context) {
    return await ReviewModel.paginate(
      {},
      {
        ...getOptionVariables(limit, page),
        populate: [
          { path: "createUser", model: "user", sort: {createdAt : -1} },
          { path: "reviewPostId", model: "post", sort: {createdAt : -1} },
        ],
        sort: {createdAt : -1},
      },
      async (err, result) => {
        if (err) {
          throw err;
        }
        return {
          pagination: {
            limit,
            page,
            totalDocs: result.totalDocs,
            totalPages: result.totalPages,
          },
          reviews: result.docs,
        };
      }
    );
  }

  static async createReview(input, user) {
    const { userId } = user;
    const user_data = await UserModel.findOne({ _id: userId });
    if (!user_data) {
      throw new ApolloError("ไม่มีผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
    }
    const { reviewPostId } = input;
    if (!user_data.history.includes(reviewPostId)) throw new ApolloError("ไม่สามารถรีวิวได้ เนื่องจากคุณไม่ได้รับการเช็คชื่อ หรือ เคยรีวิวไปแล้ว");
    const post_data = await PostModel.findOne({ _id: reviewPostId });
    if (!post_data) {
      throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
    }
    input.createUser = userId;
    const review_new = new ReviewModel(input);
    await review_new.save();

    user_data.history = user_data.history.filter(function (value, index, arr) {
      return value != reviewPostId;
    })
    user_data.reviews.push(review_new._id);
    await user_data.save();

    post_data.reviews.push(review_new._id);
    await post_data.save();
    return await ReviewModel.findOne({ _id: review_new._id }).populate([
      { path: "createUser", model: "user", sort: {createdAt : -1} },
      { path: "reviewPostId", model: "post", sort: {createdAt : -1} },
    ]);
  }
}