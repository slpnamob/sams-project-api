import {Review} from "./Review.js";
import { withUser,allUser } from "../../middleware/auth.js";
import userType from "../../configs/userType.js";

export default {
    queries: [
        {
            name: "getAllReviews",
            typeDefs: `
            type Review{
                _id: ID!,
                rate: Number!,
                comment: String,
                createUser: User!,
                reviewPostId: Post!,
            },
            type AllReview {
                pagination: Pagination,
                reviews : [Review],
            }`,
            returnTypeDef: `AllReview`,
            params: `limit: Int, page: Int`,
            action: async (root,{limit, page},context) =>
            Review.getAllReviews(root,limit,page,context),
        },
    ],
    mutations: [
        {
            name: "createReview",
            typeDefs:`
                input createReviewInput{
                    rate: Number!,
                    comment: String,
                    reviewPostId: String!,
                }`,
            returnTypeDef: `Review`,
            params: `input: createReviewInput`,
            action: async (root ,{input} , context) =>
            withUser(root, {input}, context, {
                userType: userType.BOTH,
              })(async (parent, args, context, user) =>
                Review.createReview(input, user)
              ),
        }
    ]}
    