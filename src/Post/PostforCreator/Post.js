import PostModel from "../../../models/Post.js";
import { ApolloError } from "apollo-server";
import { getOptionVariables } from "../../../configs/paginationOption.js";
import UserModel from "../../../models/User.js";
import ReviewModel from "../../../models/Review.js";
import ReportModel from "../../../models/Report.js";
import nodemailer from "nodemailer";
import { googleCalendar } from "../../../middleware/googleCalendar.js";
import processUpload from "../../../middleware/awsS3.js";

export class PostforCreator {
  static async getAllPosts(root, limit = Number.MAX_SAFE_INTEGER, page = 1, context) {
    return await PostModel.paginate(
      { is_deleted: false },
      {
        ...getOptionVariables(limit, page),
        populate: [
          { path: "createUser", model: "user", sort: { createdAt: -1 } },
          { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
          { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },
          { path: "reviews", model: "review", sort: { createdAt: -1 } },
        ],
        sort: { createdAt: -1 },
      },
      async (err, result) => {
        if (err) {
          throw err;
        }
        return {
          pagination: {
            limit,
            page,
            totalDocs: result.totalDocs,
            totalPages: result.totalPages,
          },
          posts: result.docs,
        };
      }
    );
  }

  static async getOnePost(root, input, user) {
    try {
      const { postId } = input;
      const post_data = await PostModel.findOne({ _id: postId, is_deleted: false }).populate([
        { path: "createUser", model: "user", sort: { createdAt: -1 } },
        { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
        { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },
        { path: "reviews", model: "review", sort: { createdAt: -1 },populate:{ path: "createUser" , model:"user"} },
      ]
      );
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้อยู่ในระบบ", "NO_POST_DATA");
      }
      const { userId } = user;
      if (userId == "notLogIn") {
        if (post_data.status == "open") {
          if (post_data.joinUsers.length == post_data.participantsNumber) post_data.status = "full";
          if (post_data.dateCloseApply.setHours(post_data.dateCloseApply.getHours()-7) < Date.now()) post_data.status = "closed";
          if (post_data.status != "open") await post_data.save();
        }
        post_data.canJoin = "notLogIn";
        post_data.canFav = false;
        post_data.isCreator = false;
        post_data.canReview = false;
      }
      else {
        const user_data = await UserModel.findOne({ _id: userId });
        if (!user_data) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }
        if (user_data.favs.includes(postId)) post_data.canFav = false;
        else post_data.canFav = true;
        if (post_data.status == "open") {
          if (post_data.joinUsers.length == post_data.participantsNumber) post_data.status = "full";
          if (post_data.dateCloseApply.setHours(post_data.dateCloseApply.getHours()-7) < Date.now()) post_data.status = "closed";
          if (post_data.status != "open") await post_data.save();
        }
        if (user_data.joins.includes(postId)) post_data.canJoin = "joined";
        else {
          if (post_data.status == "full") post_data.canJoin = "full";
          if (post_data.status == "closed") post_data.canJoin = "closed"
          else post_data.canJoin = "canJoin";
        }
        if (post_data.createUser._id == userId) {
          post_data.canJoin = "createUser";
          post_data.canFav = false;
          post_data.isCreator = true;
        }
        else {
          post_data.isCreator = false;
        }
        if (!user_data.history.includes(postId) || post_data.dateEnd > Date.now()) {

          post_data.canReview = false;
        }
        else { post_data.canReview = true; }
        var averageRate = 0;
        for (const element of post_data.reviews) {
          const review_data = await ReviewModel.findOne({ _id: element });
          averageRate += review_data.rate;
        }
        if(post_data.reviews.length != 0) averageRate = averageRate / post_data.reviews.length;
        post_data.avgRate = averageRate;
      }
      return post_data;
    }
    catch (err) { throw new ApolloError(err); }
  }


  static async createPost(input, user) {
    try {
      const { userId } = user;
      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      if (input.dateStart == "" || input.dateEnd == "" || input.name == "" || input.timeStart == "" || input.timeEnd == "" || input.place == "" || input.major == "" || input.dateCloseApply == "") {
        throw new ApolloError("กรุณาใส่ข้อมูลให้ครบทุกช่อง")
      }
      input.createUser = user_data._id;
      input.dateStart = input.dateStart + 'T' + input.timeStart + ":00";
      input.dateEnd = input.dateEnd + 'T' + input.timeEnd + ":00";
      input.dateCloseApply = input.dateCloseApply + ':59';
      var myDate = new Date(input.dateStart);
      const dateStart = myDate.getTime();
      myDate = new Date(input.dateEnd);
      const dateEnd = myDate.getTime();
      myDate = new Date(input.dateCloseApply);
      const dateCloseApply = myDate.getTime();
      if (dateStart >= Date.now() && dateEnd >= Date.now() && dateEnd >= dateStart && dateCloseApply <= dateStart && dateCloseApply >= Date.now()) {
        input.status = "open";
        if(input.photoHeader){
          const obj = await input.photoHeader;
          const photo = await processUpload(obj);
          input.photoHeader = photo.location;
        }
        for(const ele of user_data.posts){
          const this_post = await PostModel.findOne({ _id: ele , is_deleted : false });
          if(this_post == null) continue;
          console.log(input.dateStart,this_post.dateStart);
          if(dateStart>=this_post.dateStart && dateEnd<=this_post.dateEnd) {
            throw new ApolloError("ไม่สามารถสร้างกิจกรรมได้ เนื่องจากเวลาที่เริ่มจัดกิจกรรมตรงกับกิจกรรมที่เคยสร้างแล้ว");
          }
        }
        for(const ele of user_data.joins){
          const this_post = await PostModel.findOne({ _id: ele , is_deleted : false });
          if(this_post == null) continue;
          if(dateStart>=this_post.dateStart && dateEnd<=this_post.dateEnd) {
            throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากเวลาที่เริ่มจัดกิจกรรมตรงกับกิจกรรมที่เคยเข้าร่วมแล้ว");
          }
        }
        const post_new = new PostModel(input);
        await post_new.save();
        user_data.posts.push(post_new._id);
        await user_data.save();
        input.postId = post_new._id;
        googleCalendar.createEvent(input);
        return await PostModel.findOne({ _id: post_new._id }).populate([
          { path: "createUser", model: "user" },
        ]);;
      }
      else throw new ApolloError("วันที่ต่าง ๆ ของกิจกรรมมีข้อผิดพลาด เช่น วันที่เริ่มกิจกรรมก่อนวันที่ปิดรับสมัคร, วันที่ปิดรับสมัครได้สิ้นสุดลงแล้ว", "WRONG_DATE");
    }
    catch (err) { throw new ApolloError(err); }
  }

  static async editPost(input, user) {
    try {
      const { userId } = user;
      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      const { postId } = input;
      const post_data = await PostModel.findOne({ _id: postId, is_deleted: false });
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้อยู่ในระบบ", "NO_POST_DATA");
      }
      if (post_data.createUser != userId) {
        throw new ApolloError("คุณไม่ใช่ผู้สร้างกิจกรรม", "CANT_EDIT");
      }
      if (post_data.dateEnd.getTime() < Date.now()) {
        throw new ApolloError("กิจกรรมได้จบลงแล้ว", "CANT_EDIT");
      }

      for (var ele in input) {
        if (input[ele] == "" || input[ele] == NaN) 
        {
          if(ele != "description") throw new ApolloError("กรุณาใส่ข้อมูลให้ถูกต้อง");
        }
      }

      delete input.postId;
      if(input.photoHeader){
        const obj = await input.photoHeader;
        const photo = await processUpload(obj);
        input.photoHeader = photo.location;
      }
      if (input.dateStart != undefined || input.dateEnd != undefined || input.dateCloseApply != undefined) {
        var myDate = new Date();
        var dateNow = Date.now();
        var dateStart = new Date();
        var dateEnd = new Date();
        if (input.timeStart == undefined) input.timeStart = post_data.timeStart;
        if (input.timeEnd == undefined) input.timeEnd = post_data.timeEnd;
        if (input.dateStart != undefined || input.dateEnd != undefined) {
          if (input.dateEnd != undefined && input.dateStart != undefined) {
            input.dateStart = input.dateStart + 'T' + input.timeStart + ":00";
            input.dateEnd = input.dateEnd + 'T' + input.timeEnd + ":00";
            myDate = new Date(input.dateEnd);
            dateEnd = myDate.setHours(myDate.getHours()-7);
            myDate = new Date(input.dateStart);
            dateStart = myDate.setHours(myDate.getHours()-7);
          }
          else if (input.dateEnd != undefined) {
            input.dateEnd = input.dateEnd + 'T' + input.timeEnd + ":00";
            myDate = new Date(input.dateEnd);
            dateEnd = myDate.setHours(myDate.getHours()-7);
            myDate = new Date(post_data.dateStart);
            dateStart = myDate.setHours(myDate.getHours()-7);
          }
          else {
            input.dateStart = input.dateStart + 'T' + input.timeStart + ":00";
            myDate = new Date(post_data.dateEnd);
            dateEnd = myDate.setHours(myDate.getHours()-7);
            myDate = new Date(input.dateStart);
            dateStart = myDate.setHours(myDate.getHours()-7);
          }

          if (dateStart >= dateNow && dateEnd >= dateNow && dateEnd >= dateStart) {
          }
          else throw new ApolloError("วันที่ต่าง ๆ ของกิจกรรมมีข้อผิดพลาด เช่น วันที่จบกิจกรรมก่อนวันที่เริ่มกิจกรรม", "WRONG_DATE");
        }
        else {
          myDate = new Date(post_data.dateStart);
          dateStart = myDate.setHours(myDate.getHours()-7);
        }
        if (input.dateCloseApply != undefined) {
          input.dateCloseApply = input.dateCloseApply + ':59';
          myDate = new Date(input.dateCloseApply);
          const dateCloseApply = myDate.setHours(myDate.getHours()-7);
          if (dateCloseApply > dateNow && dateCloseApply <= dateStart) input.status = "open";
          else if (dateCloseApply <= dateStart) input.status = "closed";
          else throw new ApolloError("วันที่ต่าง ๆ ของกิจกรรมมีข้อผิดพลาด เช่น วันที่เริ่มกิจกรรมก่อนวันที่ปิดรับสมัคร", "WRONG_DATE");
        }
      }
      input.eventId = post_data.googleCalendarEventId;
      googleCalendar.updateEvent(input);

      await PostModel.findOneAndUpdate({ _id: postId, is_deleted: false }, input)

      return await PostModel.findOne({ _id: postId, is_deleted: false }).populate(
        [{ path: "createUser", model: "user", sort: { createdAt: -1 } },
        { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
        { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },
        { path: "reviews", model: "review", sort: { createdAt: -1 } },]);
    }
    catch (err) { throw new ApolloError(err); }
  }

  static async deletePost(input, user) {
    const { postId } = input;
    const post_data = await PostModel.findOne({ _id: postId, is_deleted: false });
    if (!post_data) {
      throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
    }
    const { userId } = user;
    if (post_data.createUser != userId && user.type != "admin") throw new ApolloError("คุณไม่ใช่ผู้สร้างกิจกรรม หรือเป็นผู้ดูแลระบบ");
    post_data.is_deleted = true;
    await post_data.save();
    googleCalendar.deleteEvent({ eventId: post_data.googleCalendarEventId });
    const report_data = await ReportModel.find({ reportPostId: postId });
    for (const ele of report_data) {
      ele.is_deleted = true;
      await ele.save();
    }
    return await PostModel.findOne({ _id: postId, is_deleted: true }).populate(
      [{ path: "createUser", model: "user", sort: { createdAt: -1 } },
      { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
      { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },
      { path: "reviews", model: "review", sort: { createdAt: -1 } },]);
  }


  static async attendanceCheck(input, user) {
    const { userId } = user;
    const { postId, checkedUsersId } = input;
    const post_data = await PostModel.findOne({ _id: postId, is_deleted: false });
    if (!post_data) {
      throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
    }
    if(post_data.dateStart.setHours(post_data.dateStart.getHours()-7) > Date.now()){
      throw new ApolloError("ยังไม่ถึงวันเริ่มกิจกรรม", "CAN_NOT_CHECK");
    }
    const user_data = await UserModel.findOne({ _id: userId });
    if (!user_data) {
      throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
    }
    if (post_data.checkedUsers != []) {
      for (const ele of post_data.checkedUsers) {
        const this_user = await UserModel.findOne({ _id: ele });
        if (!this_user) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }

        this_user.history = this_user.history.filter(function (value, index, arr) {
          return value != postId;
        });

        await this_user.save();
      }
      post_data.checkedUsers = [];
    }

    if (userId != post_data.createUser) {
      throw new ApolloError("คุณไม่ใช่ผู้สร้างกิจกรรม", "CAN_NOT_CHECK");
    }
    for (const ele of checkedUsersId) {
      const this_user = await UserModel.findOne({ _id: ele });
      if (!this_user) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      if (!post_data.joinUsers.includes(ele)) {
        throw new ApolloError("บุคคลที่ถูกเช็คชื่อยังไม่ได้เข้าร่วมกิจกรรม", "CAN_NOT_CHECK");
      }
      this_user.history.push(postId);
      await this_user.save();
      post_data.checkedUsers.push(ele);
    }
    await post_data.save();
    return await PostModel.findOne({ _id: postId, is_deleted: false }).populate(
      [{ path: "createUser", model: "user", sort: { createdAt: -1 } },
      { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
      { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },]);
  }

  static async sendEmail(input, user) {
    try {
      const { postId, subject, message } = input;
      const { userId } = user;

      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }

      const post_data = await PostModel.findOne({ _id: postId, is_deleted: false });
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if (userId != post_data.createUser) {
        throw new ApolloError("คุณไม่ใช่ผู้สร้างกิจกรรม", "CAN_NOT_SEND");
      }

      var receiver = '';
      for (const thisUser of post_data.joinUsers) {
        var this_user = await UserModel.findOne({ _id: thisUser });
        receiver += this_user.email + ', ';
      }


      if (receiver == '') throw new ApolloError("ยังไม่มีผู้เข้าร่วมกิจกรรม", "CAN_NOT_SEND");
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'SAMS.KMITL@gmail.com',
          pass: 'zgaejrppqoofztfn'
        }
      });

      const mailOptions = {
        from: 'SAMS.KMITL@gmail.com',
        to: receiver,
        subject: '[' + post_data.name + '] ' + subject,
        text: message
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
      return await PostModel.findOne({ _id: postId, is_deleted: false }).populate(
        [{ path: "createUser", model: "user", sort: { createdAt: -1 } },
        { path: "joinUsers", model: "user", sort: { createdAt: -1 } },
        { path: "checkedUsers", model: "user", sort: { createdAt: -1 } },]);
    }
    catch (err) { throw new ApolloError(err); }
  }

  static async forgetPassword(input) {
    try {
      const { email } = input;

      const user_data = await UserModel.findOne({ email: email });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }

      var receiver = user_data.email;

      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'SAMS.KMITL@gmail.com',
          pass: 'zgaejrppqoofztfn'
        }
      });

      const mailOptions = {
        from: 'SAMS.KMITL@gmail.com',
        to: receiver,
        subject: 'ดำเนินการเปลี่ยนรหัสผ่าน',
        text: 'คุณสามารถนำ code ต่อไปนี้ใส่ลงในช่อง code เพื่อเปลี่ยนรหัสผ่าน CODE: ' + user_data._id
      };

      var success = {};
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          success.success = false;
          throw new ApolloError(error);
        } else {
          success.success = true;
          console.log('Email sent: ' + info.response);
          return success;
        }
      });
    }
    catch (err) { throw new ApolloError(err); }
  }

}


