import {PostforCreator} from "./Post.js";
import { withUser,allUser } from "../../../middleware/auth.js";
import userType from "../../../configs/userType.js";
import processUpload from "../../../middleware/awsS3.js";

export default {
    queries: [
      {
        name: "getAllPosts",
        typeDefs: `
          type Post{
              _id: ID!,
              photoHeader: String,
              status: String,
              name: String,
              dateStart: Date,
              dateEnd: Date,
              timeStart: String,
              timeEnd: String,
              place: String,
              participantsNumber: Number,
              dateCloseApply: Date,
              major: String,
              description: String,
              createUser: User,
              joinUsers: [User],
              checkedUsers: [User],
              reviews: [Review],
          },
          type AllPost {
              pagination: Pagination,
              posts : [Post],
          }`,
        returnTypeDef: `AllPost`,
        params: `limit: Int, page: Int`,
        action: async (root,{limit, page},context) =>
        PostforCreator.getAllPosts(root,limit,page,context),
      },
      {
        name: "getOnePost",
        typeDefs: `
        input getOnePostInput{
          postId : String!
        },
        type getOnePost{
          _id: ID!,
          photoHeader: String,
          status: String!,
          name: String!,
          dateStart: Date!,
          dateEnd: Date!,
          timeStart: String!,
          timeEnd: String!,
          place: String!,
          participantsNumber: Number!,
          dateCloseApply: Date!,
          major: String!,
          description: String,
          createUser: User,
          joinUsers: [User],
          checkedUsers: [User],
          canJoin: String,
          canFav: Boolean,
          canReview: Boolean,
          avgRate: Number,
          isCreator: Boolean,
          reviews: [Review],
        },`
        ,
        params: `input: getOnePostInput`,
        returnTypeDef: `getOnePost`,
        action: async (root, {input}, context) =>
          allUser(root, {input}, context,{
            userType: userType.BOTH,
          })(async (root, args, context , user) =>
            PostforCreator.getOnePost(root,input,user)
          ),
      },
    ],
    mutations: [
        { 
            name: "createPost",
            typeDefs:`
                input createPostInput{
                    photoHeader: Upload,
                    name: String!,
                    dateStart: Date!,
                    dateEnd: Date!,
                    timeStart: String!,
                    timeEnd: String!,
                    place: String!,
                    participantsNumber: Number!,
                    dateCloseApply: Date!,
                    major: String!,
                    description: String,
                }`,
            returnTypeDef: `Post`,
            params: `input: createPostInput`,
            action: async (root ,{input} , context) =>
            withUser(root, {input}, context, {
                userType: userType.BOTH,
              })(async (parent, args, context, user) =>
                PostforCreator.createPost(input, user)
              ),
        },
        {
          name: "editPost",
          typeDefs:`
          input editPostInput{
            postId: String!,
            photoHeader: Upload,
            name: String,
            dateStart: Date,
            dateEnd: Date,
            timeStart: String,
            timeEnd: String,
            place: String,
            participantsNumber: Number,
            dateCloseApply: Date,
            major: String,
            description: String,
        }`,
          returnTypeDef: `Post`,
          params: `input: editPostInput`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforCreator.editPost(input, user)
            ),
        },
        {
          name: "deletePost",
          typeDefs:`
          input deletePostInput{
            postId : String!
          }`,
          returnTypeDef: `Post`,
          params: `input: deletePostInput`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforCreator.deletePost(input, user)
            ),
        },
        {
          name: "attendanceCheck",
          typeDefs: ` type PostDone{
            _id: ID!,
            status: String!,
            name: String!,
            dateStart: Date!,
            dateEnd: Date!,
            timeStart: String!,
            timeEnd: String!,
            place: String!,
            participantsNumber: Number!,
            dateCloseApply: Date!,
            major: String!,
            description: String,
            createUser: User,
            joinUsers: [User],
            checkedUsers: [User],
        },
          input attendanceCheckInput{
            postId : String!,
            checkedUsersId: [String!],
          }`,
          params: `input: attendanceCheckInput`,
          returnTypeDef: `PostDone`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforCreator.attendanceCheck(input, user)
            ),
        },
        {
          name: "sendEmail",
          typeDefs: `
          input sendEmailInput{
            postId: String!,
            subject: String!,
            message: String,
          }`,
          params: `input: sendEmailInput`,
          returnTypeDef: `Post`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforCreator.sendEmail(input, user)
            ),
        },
        {
          name:"uploadImage",
          typeDefs: `
          type File{
            filename: String!,
            mimetype: String!,
            encoding: String!,
            location: String,
          },
          input imageUpload{
            file: Upload!
          }`,
          params: `input: imageUpload`,
          returnTypeDef: `File`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              processUpload(input.file)
            ),
        },
        {
          name: "forgetPassword",
          typeDefs: `
          input forgetPassword{
            email: String!,
          }
          type Success{
            success: Boolean
          }`
          ,
          params: `input: forgetPassword`,
          returnTypeDef: `Success`,
          action: async (root ,{input}) =>
          PostforCreator.forgetPassword(input)
        },
    ]
}