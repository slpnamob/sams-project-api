import PostModel from "../../../models/Post.js";
import { ApolloError } from "apollo-server";
import { getOptionVariables } from "../../../configs/paginationOption.js";
import mongoose from "mongoose";
import UserModel from "../../../models/User.js";
import ReviewModel from "../../../models/Review.js";
import {googleCalendar} from "../../../middleware/googleCalendar.js";
import { google } from "googleapis";

export class PostforUser {

  static async getAllPostsByAuthen(limit = Number.MAX_SAFE_INTEGER, page = 1, user) {

    try {
      const { userId } = user;

      const post_data = await PostModel.paginate(
        { is_deleted: false },
        {
          ...getOptionVariables(limit, page),
          populate: [
            { path: "createUser", model: "user", sort: {createdAt : -1} },
            { path: "joinUsers", model: "user", sort: {createdAt : -1} },
            { path: "checkedUsers", model: "user", sort: {createdAt : -1} },
            { path: "reviews", model: "review", sort: {createdAt : -1} },
          ],
          sort: {createdAt : -1},
        },
        async (err, result) => {
          if (err) {
            throw err;
          }
          return {
            pagination: {
              limit,
              page,
              totalDocs: result.totalDocs,
              totalPages: result.totalPages,
            },
            posts: result.docs,
          };
        }
      );
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if (userId == "notLogIn") {
        post_data.posts.forEach(async ele => {
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          ele.canJoin = "notLogIn";
          ele.canFav = false;
        });
      }
      else {
        const user_data = await UserModel.findOne({ _id: userId });
        if (!user_data) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }
        for(const ele of post_data.posts){
          console.log(ele.status);
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          if (user_data.joins.includes(ele._id)) ele.canJoin = "joined";
          else {
            if (ele.status == "full") ele.canJoin = "full";
            if (ele.status == "closed") ele.canJoin = "closed"
            else ele.canJoin = "canJoin";
          }
          if (user_data.favs.includes(ele._id)) ele.canFav = false;
          else ele.canFav = true;
          if (ele.createUser._id == userId) {
            ele.canJoin = "createUser";
            ele.canFav = false;
          }
        }
      }
      return post_data;
    }
    catch (err) { throw new Error(err); }
  }

  static async getAllJoinedPosts(limit = Number.MAX_SAFE_INTEGER, page = 1, user) {
    try {
      const { userId } = user;
      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      const post_data = await PostModel.paginate(
        { is_deleted: false, joinUsers: userId},
        {
          ...getOptionVariables(limit, page),
          populate: [
            { path: "createUser", model: "user", sort: {createdAt : -1} },
            { path: "joinUsers", model: "user", sort: {createdAt : -1} },
            { path: "checkedUsers", model: "user", sort: {createdAt : -1} },
            { path: "reviews", model: "review", sort: {createdAt : -1} },
          ],
          sort: {createdAt : -1},
        },
        async (err, result) => {
          if (err) {
            throw err;
          }
          return {
            pagination: {
              limit,
              page,
              totalDocs: result.totalDocs,
              totalPages: result.totalPages,
            },
            posts: result.docs,
          };
        }
      );
      for (const ele of post_data.posts){
        if(ele.status == "open"){
          if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
          if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
          if(ele.status != "open") await ele.save();
        }
        ele.canJoin = "joined";
        if (user_data.favs.includes(ele._id)) ele.canFav = false;
        else ele.canFav = true;
        if (ele.createUser._id == userId) {
          ele.canFav = false;
        }
        if(!user_data.history.includes(ele._id) || ele.dateEnd > Date.now()){
          
          ele.canReview = false;
        }
        else{ ele.canReview = true; }
        var averageRate = 0;
        for (const element of ele.reviews){
          const review_data = await ReviewModel.findOne({_id:element});
          averageRate+=review_data.rate;
        }
        if(ele.reviews.length != 0) averageRate = averageRate/ele.reviews.length;
        ele.avgRate = averageRate;
      }
      return post_data;
    }
    catch (err) { throw new ApolloError(err); }
  }

  
  static async getPopularPosts(limit = Number.MAX_SAFE_INTEGER, page = 1, user) {
    try {
      const { userId } = user;

      const post_data = await PostModel.paginate(
        { is_deleted: false, dateCloseApply: { $gt: Date.now() } },
        {
          ...getOptionVariables(limit, page),
          populate: [
            { path: "createUser", model: "user", sort: {createdAt : -1} },
            { path: "joinUsers", model: "user", sort: {createdAt : -1} },
            { path: "checkedUsers", model: "user", sort: {createdAt : -1} },
            { path: "reviews", model: "review", sort: {createdAt : -1} },
          ],
        },
        async (err, result) => {
          if (err) {
            throw err;
          }
          return {
            pagination: {
              limit,
              page,
              totalDocs: result.totalDocs,
              totalPages: result.totalPages,
            },
            posts: result.docs,
          };
        }
      );
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      //( จำนวนผู้เข้าร่วม / จำนวนผู้ที่รับ ) / จำนวนเวลาที่เปิดรับสมัครมา
      await post_data.posts.forEach(async ele => {
        const numofJoins = ele.joinUsers.length;
        const timeOpenApply = Date.now() - ele.createdAt;
        ele.isPopular = numofJoins / timeOpenApply; //จำนวนผู้เข้าร่วม / จำนวนเวลาที่เปิดรับสมัคร
      });
      if (userId == "notLogIn") {
        await post_data.posts.forEach(async ele => {
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          ele.canJoin = "notLogIn";
          ele.canFav = false;
        });
      }
      else {
        const user_data = await UserModel.findOne({ _id: userId });
        if (!user_data) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }
        for(const ele of post_data.posts){
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          if (user_data.joins.includes(ele._id)) ele.canJoin = "joined";
          else {
            if (ele.status == "full") ele.canJoin = "full";
            if (ele.status == "closed") ele.canJoin = "closed"
            else ele.canJoin = "canJoin";
          }
          if (user_data.favs.includes(ele._id)) ele.canFav = false;
          else ele.canFav = true;
          if (ele.createUser._id == userId) {
            ele.canJoin = "createUser";
            ele.canFav = false;
          }
        }
      }
      post_data.posts = await post_data.posts.sort(function (a, b) {
        return a.isPopular - b.isPopular;
      });
      return post_data;
    }
    catch (err) { throw new Error(err); }
  }

  static async getCloseSoonPosts(limit = Number.MAX_SAFE_INTEGER, page = 1, user) {
    try {
      const date = new Date();
      const nextMonth = date.setDate(date.getDate()+30);
      const { userId } = user;

      const post_data = await PostModel.paginate(
        { is_deleted: false, dateCloseApply: {$gt: Date.now() , $lt: nextMonth }},
        {
          ...getOptionVariables(limit, page),
          populate: [
            { path: "createUser", model: "user", sort: {createdAt : -1} },
            { path: "joinUsers", model: "user", sort: {createdAt : -1} },
            { path: "checkedUsers", model: "user", sort: {createdAt : -1} },
            { path: "reviews", model: "review", sort: {createdAt : -1} },
          ],
          sort: { dateCloseApply: -1}, 
        },
        async (err, result) => {
          if (err) {
            throw err;
          }
          return {
            pagination: {
              limit,
              page,
              totalDocs: result.totalDocs,
              totalPages: result.totalPages,
            },
            posts: result.docs,
          };
        }
      );
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if (userId == "notLogIn") {
        await post_data.posts.forEach(async ele => {
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          ele.canJoin = "notLogIn";
          ele.canFav = false;
        });
      }
      else {
        const user_data = await UserModel.findOne({ _id: userId });
        if (!user_data) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }
        for(const ele of post_data.posts){
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          if (user_data.joins.includes(ele._id)) ele.canJoin = "joined";
          else {
            if (ele.status == "full") ele.canJoin = "full";
            if (ele.status == "closed") ele.canJoin = "closed"
            else ele.canJoin = "canJoin";
          }
          if (user_data.favs.includes(ele._id)) ele.canFav = false;
          else ele.canFav = true;
          if (ele.createUser._id == userId) {
            ele.canJoin = "createUser";
            ele.canFav = false;
          }
        }
      }
      return post_data;
    }
    catch (err) { throw new ApolloError(err); }
  }

  static async getUpComingPosts(limit = Number.MAX_SAFE_INTEGER, page = 1, user) {
    try {
      const { userId } = user;
      const date = new Date();
      const nextMonth = date.setDate(date.getDate()+30);

      const post_data = await PostModel.paginate(
        { is_deleted: false, dateStart:{$gt: Date.now() , $lt: nextMonth } },
        {
          ...getOptionVariables(limit, page),
          populate: [
            { path: "createUser", model: "user", sort: {createdAt : -1} },
            { path: "joinUsers", model: "user", sort: {createdAt : -1} },
            { path: "checkedUsers", model: "user", sort: {createdAt : -1} },
            { path: "reviews", model: "review", sort: {createdAt : -1} },
          ],
          sort: { dateStart: -1 },
        },
        async (err, result) => {
          if (err) {
            throw err;
          }
          return {
            pagination: {
              limit,
              page,
              totalDocs: result.totalDocs,
              totalPages: result.totalPages,
            },
            posts: result.docs,
          };
        }
      );
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if (userId == "notLogIn") {
        await post_data.posts.forEach(async ele => {
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          ele.canJoin = "notLogIn";
          ele.canFav = false;
        });
      }
      else {
        const user_data = await UserModel.findOne({ _id: userId });
        if (!user_data) {
          throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
        }
        for(const ele of post_data.posts){
          if(ele.status == "open"){
            if(ele.joinUsers.length==ele.participantsNumber) ele.status = "full";
            if(ele.dateCloseApply.setHours(ele.dateCloseApply.getHours()-7) < Date.now()) ele.status = "closed";
            if(ele.status != "open") await ele.save();
          }
          if (user_data.joins.includes(ele._id)) ele.canJoin = "joined";
          else {
            if (ele.status== "full") ele.canJoin = "full";
            if (ele.status == "closed") ele.canJoin = "closed"
            else ele.canJoin = "canJoin";
          }
          if (user_data.favs.includes(ele._id)) ele.canFav = false;
          else ele.canFav = true;
          if (ele.createUser._id == userId) {
            ele.canJoin = "createUser";
            ele.canFav = false;
          }
        }
      }
      return post_data;
    }
    catch (err) { throw new ApolloError(err); }
  }

  static async joinPost(input, user) {
    try{
      const { userId } = user;
      const user_data = await UserModel.findOne({ _id: userId });
      if (!user_data) {
        throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
      }
      const { postId } = input;
      const post_data = await PostModel.findOne({ _id: postId , is_deleted : false });
      if (!post_data) {
        throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
      }
      if (post_data.status == "closed" || post_data.status == "full") throw new ApolloError("กิจกรรมมีผู้เข้าร่วมเต็มจำนวน หรือ ปิดรับสมัครแล้ว", "CANT_JOIN");
      var myDate = new Date(post_data.dateCloseApply);
      const dateCloseApply = myDate.setHours(myDate.getHours()-7);
      if (dateCloseApply <= Date.now()) {
        post_data.status = "closed";
        await post_data.save();
        throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากกิจกรรมนี้ได้จบลงแล้ว", "CANT_JOIN");
      }
      if (post_data.joinUsers.length > post_data.participantsNumber) {
        post_data.status = "full";
        await post_data.save();
        throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากกิจกรรมมีผู้เข้าร่วมเต็มจำนวนแล้ว", "CANT_JOIN");
      }
      if (post_data.joinUsers.includes(user_data._id)) {
        throw new ApolloError("คุณเคยเข้าร่วมแล้ว", "CANT_JOIN");
      }
      if (post_data.createUser == userId) {
        throw new ApolloError("ไม่สามารถเข้าร่วมกิจกรรมของตนเองได้", "CANT_JOIN");
      }
      if (post_data.major != "ทั้งหมด" && post_data.major != user_data.major && user_data.type != "บุคลากรภายใน" && user_data.type != "อาจารย์") {
        throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากคณะของคุณไม่ตรงตามที่กำหนด", "CANT_JOIN");
      }
      for(const ele of user_data.posts){
        const this_post = await PostModel.findOne({ _id: ele , is_deleted : false });
        if(this_post == null) continue;
        if(post_data.dateStart>=this_post.dateStart && post_data.dateEnd<=this_post.dateEnd) {
          console.log(this_post);
          throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากเวลาที่เริ่มจัดกิจกรรมตรงกับกิจกรรมที่เคยสร้างแล้ว");
        }
      }
      for(const ele of user_data.joins){
        const this_post = await PostModel.findOne({ _id: ele , is_deleted : false });
        if(this_post == null) continue;
        if(post_data.dateStart>=this_post.dateStart && post_data.dateEnd<=this_post.dateEnd) {
          console.log(this_post);
          throw new ApolloError("ไม่สามารถเข้าร่วมได้ เนื่องจากเวลาที่เริ่มจัดกิจกรรมตรงกับกิจกรรมที่เคยเข้าร่วมแล้ว");
        }
      }
      user_data.joins.push(post_data._id);
      await user_data.save();
      post_data.joinUsers.push(user_data._id);
      await post_data.save();
      googleCalendar.joinEvent({eventId: post_data.googleCalendarEventId, userEmail: user_data.email});
      return await UserModel.findOne({ _id: userId }).populate(
        [{ path: "posts", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
        { path: "joins", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
        { path: "favs", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },]
      );
    }
    catch(err){throw new ApolloError(err);}
  }

  static async unjoinPost(input, user) {
    const { userId } = user;
    const { postId } = input;
    const user_data = await UserModel.findOne({ _id: userId });
    if (!user_data) {
      throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
    }
    if (!user_data.joins.includes(postId)) {
      throw new ApolloError("ไม่สามารถยกเลิกการเข้าร่วมได้ เนื่องจากคุณไม่ได้เข้าร่วมกิจกรรมนี้", "CANT_UNJOIN");
    }
    const post_data = await PostModel.findOne({ _id: postId , is_deleted : false });
    if (!post_data) {
      throw new ApolloError("ไม่มีกิจกรรมนี้ในระบบ", "NO_POST_DATA");
    }
    if (!post_data.joinUsers.includes(user_data._id)) {
      throw new ApolloError("ไม่สามารถยกเลิกการเข้าร่วมได้ เนื่องจากคุณไม่ได้เข้าร่วมกิจกรรมนี้", "CANT_UNJOIN");
    }
    if(post_data.dateEnd.getTime()<Date.now()){
      throw new ApolloError("ไม่สามารถยกเลิกการเข้าร่วมได้ เนื่องจากกิจกรรมนี้ได้จบลงแล้ว", "CANT_UNJOIN");
    }
    user_data.joins = await user_data.joins.filter(function (value, index, arr) {
      return value != postId;
    });
    await user_data.save();
    post_data.joinUsers = await post_data.joinUsers.filter(function (value, index, arr) {
      return value != userId;
    });
    await post_data.save();
    googleCalendar.unjoinEvent({eventId: post_data.googleCalendarEventId, userEmail: user_data.email});
    return await UserModel.findOne({ _id: userId }).populate(
      [{ path: "posts", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "joins", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "favs", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },]
    );
  }

  static async favPost(input, user) {
    const { userId } = user;
    const { postId } = input;
    const user_data = await UserModel.findOne({ _id: userId });
    if (!user_data) {
      throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
    }
    if (user_data.favs.includes(postId)) {
      throw new ApolloError("คุณได้กดชื่นชอบกิจกรรมนี้แล้ว", "CANT_JOIN");
    }
    user_data.favs.push(postId);
    await user_data.save();
    return await UserModel.findOne({ _id: userId }).populate(
      [{ path: "posts", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "joins", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "favs", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },]
    );
  }

  static async unfavPost(input, user) {
    const { userId } = user;
    const { postId } = input;
    const user_data = await UserModel.findOne({ _id: userId });
    if (!user_data) {
      throw new ApolloError("ไม่มีข้อมูลผู้ใช้งานนี้ในระบบ", "NO_USER_DATA");
    }
    if (!user_data.favs.includes(postId)) {
      throw new ApolloError("คุณไม่ได้กดชื่นชอบกิจกรรมนี้", "CANT_UNFAVORITE");
    }
    user_data.favs = user_data.favs.filter(function (value, index, arr) {
      return value != postId;
    });
    await user_data.save();
    return await UserModel.findOne({ _id: userId }).populate(
      [{ path: "posts", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "joins", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },
      { path: "favs", model: "post", match: { is_deleted: false }, sort: {createdAt : -1} },]
    );
  }
}
