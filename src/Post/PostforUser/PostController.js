import {PostforUser} from "./Post.js";
import { withUser,allUser } from "../../../middleware/auth.js";
import userType from "../../../configs/userType.js";

export default {
    queries: [
      {
        name: "getAllPostsByAuthen",
        typeDefs:`
        type PostAuthen{
          _id: ID!,
          photoHeader: String,
          status: String,
          name: String,
          dateStart: Date,
          dateEnd: Date,
          timeStart: String,
          timeEnd: String,
          place: String,
          participantsNumber: Number,
          dateCloseApply: Date,
          major: String,
          description: String,
          createUser: User,
          joinUsers: [User],
          canJoin: String,
          canFav: Boolean,
          reviews: [Review],
          checkedUsers: [User],
        },
        type AllPostAuthen {
            pagination: Pagination,
            posts : [PostAuthen],
        }`,
        returnTypeDef: `AllPostAuthen`,
        params: `limit: Int, page: Int`,
        action: async (root, params, context) =>
          allUser(root, params, context,{
            userType: userType.BOTH,
          })(async (root, {limit,page}, context , user) =>
            PostforUser.getAllPostsByAuthen(limit,page,user)
          ),
      },
      {
        name: "getAllJoinedPosts",
        typeDefs:`
        type JoinedPosts{
          _id: ID!,
          photoHeader: String,
          status: String,
          name: String,
          dateStart: Date,
          dateEnd: Date,
          timeStart: String,
          timeEnd: String,
          place: String,
          participantsNumber: Number,
          dateCloseApply: Date,
          major: String,
          description: String,
          createUser: User,
          joinUsers: [User],
          reviews: [Review],
          avgRate: Number,
          canJoin: String,
          canFav: Boolean,
          canReview: Boolean,
          checkedUsers: [User],
        },
        type AllJoinedPosts {
          pagination: Pagination,
          posts : [JoinedPosts],
        }`,
        returnTypeDef: `AllJoinedPosts`,
        params: `limit: Int, page: Int`,
        action: async (root, params, context) =>
          withUser(root, params, context,{
            userType: userType.BOTH,
          })(async (root, {limit,page}, context , user) =>
            PostforUser.getAllJoinedPosts(limit,page,user)
          ),
      },
      {
        name: "getPopularPosts",
        typeDefs: ``,
        returnTypeDef: `AllPostAuthen`,
        params: `limit: Int, page: Int`,
        action: async (root, params, context) =>
          allUser(root, params, context,{
            userType: userType.BOTH,
          })(async (root, {limit,page}, context , user) =>
            PostforUser.getPopularPosts(limit,page,user)
          ),
      },
      {
        name: "getCloseSoonPosts", //กี่อันถึงวันไหน
        typeDefs: ``,
        returnTypeDef: `AllPostAuthen`,
        params: `limit: Int, page: Int`,
        action: async (root, params, context) =>
          allUser(root, params, context,{
            userType: userType.BOTH,
          })(async (root, {limit,page}, context , user) =>
            PostforUser.getCloseSoonPosts(limit,page,user)
          ),
      },
      {
        name: "getUpComingPosts", //กี่อันถึงวันไหน
        typeDefs: ``,
        returnTypeDef: `AllPostAuthen`,
        params: `limit: Int, page: Int`,
        action: async (root, params, context) =>
          allUser(root, params, context,{
            userType: userType.BOTH,
          })(async (root, {limit,page}, context , user) =>
            PostforUser.getUpComingPosts(limit,page,user)
          ),
      }

    ],
    mutations: [
        
        {
          name: "joinPost",
          typeDefs: `
              input joinPostInput{
                  postId : String!
              }
          `,
          params: `input: joinPostInput`,
          returnTypeDef: `User`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforUser.joinPost(input, user)
            ),
        },
        {
          name: "unjoinPost",
          typeDefs: `
          `,
          params: `input: joinPostInput`,
          returnTypeDef: `User`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforUser.unjoinPost(input, user)
            ),
        },
        {
          name: "favPost",
          typeDefs: ``,
          params: `input: joinPostInput`,
          returnTypeDef: `User`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforUser.favPost(input, user)
            ),
        },
        {
          name: "unfavPost",
          typeDefs: ``,
          params: `input: joinPostInput`,
          returnTypeDef: `User`,
          action: async (root ,{input} , context) =>
          withUser(root, {input}, context, {
              userType: userType.BOTH,
            })(async (parent, args, context, user) =>
              PostforUser.unfavPost(input, user)
            ),
        },
        //report
    ]
}