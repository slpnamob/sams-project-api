import { ApolloServer, gql, ApolloError} from "apollo-server-express";
import mongoose from "mongoose";
import appServices from "./src/index.js";
import { v4 as uuidv4 } from "uuid";
import { decodedToken } from "./configs/decodedToken.js";
import express from "express";
import {graphqlUploadExpress} from "graphql-upload";


require("dotenv").config();

try {
  mongoose.Promise = global.Promise;
  mongoose
    .connect(process.env.API_MONGO_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      
    })
    .then((res) => console.log("Database connected."))
    .catch((e) => console.log(e));

  const typeDefs = { queries: [], mutations: [], typeDefs: [], resolvers: [] };
  appServices.forEach((service) => {
    service.queries.forEach((value) => {
      typeDefs.resolvers.push({
        Query: {
          [value.name]: value.action,
        },
      });
      typeDefs.typeDefs.push(value.typeDefs);
      // typeDefs.queries.push(`${value.name}:${value.returnTypeDef}`);
      typeDefs.queries.push(
        `${value.name}${value.params ? `(${value.params})` : ""}: ${value.returnTypeDef
        }`
      );
    });

    service.mutations.forEach((value) => {
      typeDefs.resolvers.push({
        Mutation: {
          [value.name]: value.action,
        },
      });
      typeDefs.typeDefs.push(value.typeDefs);
      typeDefs.mutations.push(
        `${value.name}${value.params ? `(${value.params})` : ""}: ${value.returnTypeDef
        }`
      );
    });
  });

  const server = new ApolloServer({
    uploads: false,
    introspection:true,
    playground:true,
    typeDefs: gql`
        scalar Date
        scalar Number
        scalar Upload
  
        ${typeDefs.typeDefs.join("\r\n")}
              
        type Query {
          ${typeDefs.queries.join("\r\n")}
        }
  
        type Mutation {
          ${typeDefs.mutations.join("\r\n")}
        }
      `,
    resolvers: typeDefs.resolvers,
    context: async ({ req, res }) => {
      //req._id = uuidv4();
      const token = req.headers.authorization;
      let jwtDecoded;
      let isLoggedIn = false;
      let userType;
      let user;
      if (token) {
        try {
          jwtDecoded = await decodedToken(token);
          const date = new Date();
          const threeDayAgo = date.setDate(date.getDate() - 3);
          if (jwtDecoded.dateToken > threeDayAgo) {
            isLoggedIn = true;
            if (jwtDecoded.type == "admin") userType = "ADMIN";
            else userType = "USER";
          }
          else {
            isLoggedIn = false;
          }
          user = jwtDecoded;
          if (user.iat) delete user.iat;
          delete user.dateToken;
        }
        catch (error) {
          console.log(error);
        }
      }

      return {
        req,
        res,
        isLoggedIn,
        user,
        userType,
      };
    },
  });

  const app = express();
  app.use(graphqlUploadExpress({ maxFileSize: 1000000000, maxFiles: 10 }));
  server.applyMiddleware({app});

  /*app.use((req, res) => {
    res.status(200);
    res.send('Hello!');
    res.end();
  });*/
   // The `listen` method launches a web server.
  /*server.listen({ port: process.env.PORT || 4000 }).then(({url}) => {
     console.log(`🚀  Server ready at ${url} `);
   });*/

   app.listen({ port: process.env.PORT || 4000 }, () => console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}  `));

 //export default server.createHandler();

}
catch (error) {
  throw new ApolloError(error);
}