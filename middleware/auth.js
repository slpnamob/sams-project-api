import { ApolloError } from "apollo-server";
import userType from "../configs/userType.js";

export const withUser = (...params) => {
    if (!params[2].isLoggedIn) {
      throw new ApolloError("กรุณาเข้าสู่ระบบ","MUST_AUTHENTICATE");
    }
    if (
      params[2].userType !== params[3].userType &&
      params[3].userType !== userType.BOTH
    ) {
      if (params[3].userType === "ADMIN")
        throw new ApolloError("เฉพาะผู้ดูแลระบบเท่านั้น","ADMIN_ONLY");
      else
        throw new ApolloError("User only","USER_ONLY");
    }
    return async (nextFn) => {
      params[3] = { ...params[2].user };
      return nextFn(...params);
    };
  };
  
  export const allUser = (...params) => {
    if (!params[2].isLoggedIn) {
      params[2].userType = "USER";
      if(params[2].user == undefined) params[2].user = {};
      params[2].user.userId = "notLogIn";
    }
    if (
      params[2].userType !== params[3].userType &&
      params[3].userType !== userType.BOTH
    ) {
      if (params[3].userType === "ADMIN")
        throw new ApolloError("เฉพาะผู้ดูแลระบบเท่านั้น","ADMIN_ONLY");
      else
        throw new ApolloError("User only","USER_ONLY");
    }
    return async (nextFn) => {
      params[3] = { ...params[2].user };
      return nextFn(...params);
    };
  };