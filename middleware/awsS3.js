const aws = require('aws-sdk');
const {v4: uuid} = require('uuid');
require("dotenv").config();


let s3 = new aws.S3({
    credentials: {
        accessKeyId: process.env.ACCESS_KEY_ID,
        secretAccessKey: process.env.SECRET_ACCESS_KEY
    },
    region: process.env.REGION,
    params : {
        ACL : 'public-read',
        Bucket : process.env.AWS_BUCKET
    }
});

const processUpload = async (file)=>{
    const obj = await file[Object.keys(file)[0]];
    console.log(obj.file);
    const {createReadStream, mimetype, encoding, filename} = await obj.file;
    const stream = await createReadStream();
    const {Location} = await s3.upload({
        Body: stream,
        Key: `${uuid()}${filename}`,
        ContentType: mimetype
    }).promise();
    return new Promise((resolve,reject)=>{
        if (Location){
            resolve({
                mimetype,filename,
                location: Location, encoding
            })
        }else {
            reject({
                mimetype: "Upload failed.",filename: "Upload failed.",
                location: "Upload failed.", encoding: "Upload failed."
            })
        }
    })
}

module.exports = processUpload;