import fs from "fs";
import readline from "readline";
import { google } from "googleapis";
import { auth } from "googleapis/build/src/apis/abusiveexperiencereport/index.js";
import PostModel from "../models/Post.js";

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getAccessToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
* Get and store new token after prompting for user authorization, and then
* execute the given callback with the authorized OAuth2 client.
* @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
* @param {getEventsCallback} callback The callback for the authorized client.
*/
function getAccessToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}



function authorization(callback) {
    // Load client secrets from a local file.
    fs.readFile('./credentials.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        // Authorize a client with credentials, then call the Google Calendar API.
        authorize(JSON.parse(content), callback);
    });
}

async function createEvent(auth) {

    const calendar = google.calendar({ version: 'v3', auth });
    var event = {
        'summary': input.name,
        'location': input.place,
        'description': input.description,
        'start': {
            'dateTime': input.dateStart,
            'timeZone': 'Asia/Bangkok',
        },
        'end': {
            'dateTime': input.dateEnd,
            'timeZone': 'Asia/Bangkok',
        },
        'reminders': {
            'useDefault': false,
            'overrides': [
                { 'method': 'email', 'minutes': 24 * 60 },
                { 'method': 'popup', 'minutes': 10 },
            ],
        },
        'guestsCanInviteOthers': false,

    };


    calendar.events.insert({
        auth: auth,
        calendarId: 'primary',
        sendUpdates: 'all',
        resource: event,
    }, async function (err, event) {
        if (err) {
            console.log('There was an error contacting the Calendar service: ' + err);
            return;
        }
        else {
            const post_data = await PostModel.findOne({ _id: input.postId, is_deleted: false });
            if (!post_data) {
                throw new ApolloError("ไม่มีกิจกรรมนี้อยู่ในระบบ", "NO_POST_DATA");
            }
            post_data.googleCalendarEventId = event.data.id;
            await post_data.save();
        
            console.log('Event created: %s', event.data.htmlLink);
        }
    });


}


function deleteEvent(auth) {
    const calendar = google.calendar({ version: 'v3', auth });
    calendar.events.delete({
        auth: auth,
        calendarId: 'primary',
        sendUpdates: 'all',
        eventId: eventId,
    }, function (err, event) {
        if (err) {
            console.log('There was an error contacting the Calendar service (can not delete): ' + err);
            return;
        }
        console.log('Delete event: %s', event.data.htmlLink);
    });

}

async function joinEvent(auth) {
    const calendar = google.calendar({ version: 'v3', auth });
    var event_data = await calendar.events.get({
        auth: auth,
        calendarId: 'primary',
        eventId: eventId,
    });
    if(event_data.data.attendees == undefined) event_data.data.attendees = [];
    event_data.data.attendees.push({"email" : userEmail});
    calendar.events.patch({
        auth: auth,
        calendarId: 'primary',
        eventId: eventId,
        sendNotifications: true,
        resource: event_data.data,
    },async function (err, update) {
        if (err) {
            console.log('There was an error contacting the Calendar service (cannot update): ' + err);
            return;
        }
        console.log('Update event: %s', update.data.htmlLink);
    });
}

async function unjoinEvent(auth) {
    const calendar = google.calendar({ version: 'v3', auth });
    var event_data = await calendar.events.get({
        auth: auth,
        calendarId: 'primary',
        eventId: eventId,
    });
    event_data.data.attendees = await event_data.data.attendees.filter(function (value, index, arr) {
        return value.email != userEmail;
      });
    calendar.events.patch({
        auth: auth,
        calendarId: 'primary',
        eventId: eventId,
        resource: event_data.data,
    },async function (err, update) {
        if (err) {
            console.log('There was an error contacting the Calendar service (cannot update): ' + err);
            return;
        }
        console.log('Update event: %s', update.data.htmlLink);
    });
}

async function updateEvent(auth){
    const calendar = google.calendar({ version: 'v3', auth });
    var event_data = await calendar.events.get({
        auth: auth,
        calendarId: 'primary',
        eventId: input.eventId,
    });
    if(input.place != undefined) event_data.data.location = input.place;
    if(input.name != undefined) event_data.data.summary = input.name;
    if(input.description != undefined) event_data.data.description = input.description;
    if(input.dateStart != undefined) event_data.data.start.dateTime = input.dateStart;
    if(input.dateEnd != undefined) event_data.data.end.dateTime = input.dateEnd;
    calendar.events.patch({
        auth: auth,
        calendarId: 'primary',
        eventId: input.eventId,
        sendNotifications: true,
        resource: event_data.data,
    },async function (err, update) {
        if (err) {
            console.log('There was an error contacting the Calendar service (cannot update): ' + err);
            return;
        }
        console.log('Update event: %s', update.data.htmlLink);
    });
}


export class googleCalendar {
    static async createEvent(inputCreateEvent) {
        global.input = inputCreateEvent;
        authorization(createEvent);
    }
    static async deleteEvent(input) {
        global.eventId = input.eventId;
        authorization(deleteEvent);
    }
    static async joinEvent(input) {
        global.eventId = input.eventId;
        global.userEmail = input.userEmail;
        authorization(joinEvent);
    }
    static async unjoinEvent(input) {
        global.eventId = input.eventId;
        global.userEmail = input.userEmail;
        authorization(unjoinEvent);
    }
    static async updateEvent(inputUpdateEvent){
        global.input = inputUpdateEvent;
        authorization(updateEvent);
    }
}