import mongoose from "mongoose";
const mongoosePaginate = require("mongoose-paginate-v2");

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        type: { type: String, required: [true, "type(student,professor,staff,etc) is required"]},
        name: { type: String, required: [true, "name is required"]},
        studentId: { type: String }, 
        major: { type: String }, 
        phoneNumber: { type: String, required: [true, "Phone number is required"]},
        email: { type: String, required: [true, "email is required"],  unique:true},
        password: {type: String, required: [true, "password is required"]},
        posts: [{type: Schema.Types.ObjectId, ref: "post"}],
        joins: [{type: Schema.Types.ObjectId, ref: "post"}],
        favs: [{type: Schema.Types.ObjectId, ref: "post"}],
        history: [{type: Schema.Types.ObjectId, ref: "post"}],
        reviews: [{type: Schema.Types.ObjectId, ref: "review"}],
    },
    { timestamps: true }
  );

userSchema.plugin(mongoosePaginate);
const userModel = mongoose.model("user", userSchema , 'Users');
export default userModel;