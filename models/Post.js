import mongoose from "mongoose";
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;

const schema = new mongoose.Schema(
    {
        photoHeader: { type: String},
        photoEtc: [{type: Buffer}],
        status: { type:String, required: [true, "Status('coming soon','open','closed','full') is required"]},
        name: { type: String, required: [true, "Post name is required"], unique: true},
        dateStart: { type: Date, required: [true, "Start date is required"]},
        dateEnd: { type: Date, required: [true, "End date is required"]},
        timeStart: { type: String, required: [true, "Start time is required"]},
        timeEnd: { type: String, required: [true, "End time is required"]},
        place: { type: String, required: [true, "Place is required"]},
        participantsNumber: { type: Number, required: [true, "Numbers of participants is required"]},
        dateCloseApply: {type: Date, required: [true, "Close apply date is required"]},
        major: {type: String, required: [true, "Major is required"]},
        description: {type:String},
        is_deleted: {type:Boolean, default:false},
        createUser: {type: Schema.Types.ObjectId, ref: "user"},
        joinUsers: [{type: Schema.Types.ObjectId, ref: "user"}],
        checkedUsers: [{type: Schema.Types.ObjectId, ref: "user"}],
        reviews: [{type: Schema.Types.ObjectId, ref: "review"}],
        googleCalendarEventId: {type: String},
    },
    { timestamps: true }
);

schema.plugin(mongoosePaginate);

const Post = mongoose.model("post", schema, "Posts");

export default Post;