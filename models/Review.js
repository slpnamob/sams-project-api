import mongoose from "mongoose";
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;

const schema = new mongoose.Schema(
    {
        rate: {type: Number},
        comment: { type: String},
        createUser: {type: Schema.Types.ObjectId, ref: "user"},
        reviewPostId: {type: Schema.Types.ObjectId, ref: "post"},
    },
    { timestamps: true }
);

schema.plugin(mongoosePaginate);

const Review = mongoose.model("review", schema, "Reviews");

export default Review;