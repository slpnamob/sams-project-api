import mongoose from "mongoose";
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;

const schema = new mongoose.Schema(
    {
        comment: { type: String},
        createUser: {type: Schema.Types.ObjectId, ref: "user"},
        reportPostId: {type: Schema.Types.ObjectId, ref: "post"},
        is_deleted: {type:Boolean, default: false},
    },
    { timestamps: true }
);

schema.plugin(mongoosePaginate);

const Report = mongoose.model("report", schema, "Reports");

export default Report;